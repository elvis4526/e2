(ns e2.core
  (:gen-class)
  (:use [compojure.route]
        [compojure.core]
        [compojure.handler]
        [org.httpkit.server]
        [hiccup.core]
        [ring.middleware.defaults]
        ))

(defn show-root [req]
  (html [:html
         [:body
          [:p "Root page"]
          ]
         ]))

(defn handler [req]
  (with-channel req channel
    (if (open? channel)
      (do
        (println "Channel is open")
        (cond (websocket? channel) (println "This is a websocket connection.")
              :else (println "This is an HTTP connection"))
        
        (on-receive channel (fn [data]
                              (println "Just received:" data)))
        (on-close channel (fn [status]
                            (println "Channel is closed." status)))
        ))))

(defroutes myroutes
  (GET "/" [] show-root)
  (GET "/async" [] handler)
  )

(defn -main
  [& args]
  (println "starting server")
  (run-server myroutes {:port 8080})
  )

