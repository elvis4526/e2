(defproject e2 "0.1.0-SNAPSHOT"
  :description "Async server with compojure and http-kit"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/tools.nrepl "0.2.9"]
                 [compojure "1.3.2"]
                 [http-kit "2.1.16"]
                 [hiccup "1.0.5"]
                 [ring "1.3.2"]
                 [ring/ring-defaults "0.1.4"]]

  :plugins [[cider/cider-nrepl "0.9.0-SNAPSHOT"]]
                 
  :main ^:skip-aot e2.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})

